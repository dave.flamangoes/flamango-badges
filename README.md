# flamango-badges

Static badges for adding to projects

## Pride Badges

Used by LGBT individuals and allies as an outward symbol of their identity or support.

### Pride - Vertical Stripes

The pride badge in vertical stripe form ![pride-badge](https://gitlab.com/dave.flamangoes/flamango-badges/raw/main/pride-badges/pride-badge.svg "Multi-coloured Pride badge with vertical stripes")

#### Markdown 

`![pride-badge](https://gitlab.com/dave.flamangoes/flamango-badges/raw/main/pride-badges/pride-badge.svg "Multi-coloured Pride badge with vertical stripes")`

#### HTML 
`<img src="https://gitlab.com/dave.flamangoes/flamango-badges/raw/main/pride-badges/pride-badge.svg" alt="Multi-coloured Pride badge with vertical stripes">`

#### Direct link
SVG `https://gitlab.com/dave.flamangoes/flamango-badges/raw/main/pridge-badges/pride-badge.svg`

Alternative png link `https://i.postimg.cc/7PWMBC6Z/pride-badge.png`

### Pride - Horizantal Stripes

The pride badge in horizantal stripe form ![pride-badge](https://gitlab.com/dave.flamangoes/flamango-badges/raw/main/pride-badges/pride-badge-horizantal.svg "Multi-coloured Pride badge with horizantal stripes")

#### Markdown 

`[![pride-badge](https://gitlab.com/dave.flamangoes/flamango-badges/raw/main/pride-badges/pride-badge-horizantal.svg "Multi-coloured Pride badge with horizantal stripes")]`

#### HTML 
`<img src="https://gitlab.com/dave.flamangoes/flamango-badges/raw/main/pride-badges/pride-badge-horizantal.svg" alt="Multi-coloured Pride badge with horizantal stripes">`

#### Direct link

SVG `https://gitlab.com/dave.flamangoes/flamango-badges/raw/main/pride-badges/pride-badge-horizantal.svg`

Alternative png link `https://i.postimg.cc/ZK5y5ZbX/pride-badge-horizantal.png`
